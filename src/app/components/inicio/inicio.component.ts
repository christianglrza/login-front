import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  usuario:any

  constructor() { }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem('usuario'))
    console.log(this.usuario);
  }


}

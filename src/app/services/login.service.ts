import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url:string = "https://login-back-d10.herokuapp.com/login"

  constructor(private http:HttpClient) { }

  login(usuario:any):Observable<any>{
    return this.http.post(this.url,usuario)
  }

  guardarUsuario(usuario){
    localStorage.setItem('usuario',JSON.stringify(usuario))
  }
}